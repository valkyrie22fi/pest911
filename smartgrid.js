var smartgrid = require('smart-grid');

var settings = {
    outputStyle: 'scss', /* less || scss || sass || styl */
    columns: 12, /* number of grid columns */
    offset: '0px', /* gutter width px || % */
    mobileFirst: false, /* mobileFirst ? 'min-width' : 'max-width' */
    oldSizeStyle: false,
    container: {
        maxWidth: '1330px', /* max-width оn very large screen */
        fields: '30px' /* side fields */
    },
    breakPoints: {
        lg: {
            width: '1330px', /* -> @media (max-width: 1100px) */
            fields: '30px', /* side fields */
            offset: '0px'
        },
        md: {
            width: '1020px',
            fields: '30px', /* set fields only if you want to change container.fields */
            offset: '0px'
        },
        sm: {
            width: '768px',
            fields: '10px',
            offset: '0px'
        }
    }
};

smartgrid('./src/scss', settings);